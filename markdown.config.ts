import { rehypeHeadingIds } from "@astrojs/markdown-remark";
import remarkToc from "remark-toc";
import rehypeToc from "rehype-toc";

export default {
  remarkPlugins: [[remarkToc, { tight: true, ordered: true }]],
  rehypePlugins: [
    rehypeHeadingIds,
    [
      rehypeToc,
      {
        headings: ["h1", "h2", "h3"],
        cssClasses: {
          toc: "toc",
          link: "page-link",
        },
      },
    ],
  ],
  shikiConfig: {
    theme: "dark-plus",
    langs: [],
    wrap: true,
  },
};







