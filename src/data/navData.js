const navData = [
    {
    name: "Home",
    path: "/",
  },
  {
    name: "About",
    path: "/about/",
  },
  {
    name: "Contact",
    path: "/contact/",
  },
]

export default navData;
