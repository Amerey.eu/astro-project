import { defineConfig } from "astro/config";
import image from "@astrojs/image";
import sitemap from "@astrojs/sitemap";
import astroI18next from "astro-i18next";
import compress from "astro-compress";
import markdownConfig from "./markdown.config";

// https://astro.build/config
export default defineConfig({
  markdown: markdownConfig,
  site: "https://test.amerey.eu",
  integrations: [
    image({
      serviceEntryPoint: "@astrojs/image/sharp",
    }),
    sitemap(),
    astroI18next(),
    compress(),
  ],
});

